var tares =[];
localforage.setItem('tareas', []);
$(document).on('click', '.todo__header .fa-plus', function() {
  $('.todo__input').fadeToggle();
});

$(document).on('click', '.todo__item', function(){
  $(this).toggleClass('todo__item--completed');
});

$(document).on('click', '.todo__remove', function(){
  $(this).parent().fadeOut(500, function(){
    $(this).remove();
  });
});

$('.todo__input').keypress(function(event){
  if(event.which === 13){

    var todoText = $(this).val();
    $(this).val("");
    $(".todo__list").append("<li class='todo__item'><span class='todo__remove'><i class='fa fa-trash'></i> </span>" + todoText + "</li>");
    // guardar tarea en localStorage
    tares.push(todoText);
  };
});

localStorage.setItem("nombre", "Jesús");
localStorage.getItem("nombre");
localStorage.removeItem("nombre");
 // Borarr toda la base de datos
 localStorage.clear();
